"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from . import views


urlpatterns = [
    re_path(r'^admin/', admin.site.urls ),
    re_path(r'^$', views.homepage , name="homepage"),
    re_path(r'^gallery/', views.gallery , name="gallery"),
    re_path(r'^portfolio/', views.portfolio , name="portfolio"),
    re_path(r'^contact/', views.contact , name="contact"),
    re_path(r'^aboutme/', views.aboutme , name="aboutme"),
    re_path(r'^addform/', include('AddForm.urls'), name="addform"),
    re_path(r'^viewtodo/', include('ViewTodo.urls'), name="viewtodo"),
]
