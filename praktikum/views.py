from django.shortcuts import render,redirect

def homepage(request):
    return render(request,"homepage.html")


def gallery(request):
    return render(request,"gallery.html")

def aboutme(request):
    return render(request,"aboutme.html")

def contact(request):
    return render(request,"contact.html")

def portfolio(request):
    return render(request,"portfolio.html")




