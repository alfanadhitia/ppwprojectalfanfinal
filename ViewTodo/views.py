from django.shortcuts import render,redirect,HttpResponseRedirect
from ViewTodo.models import TodoItem
# Create your views here.


def ViewTodo (request): 
    todos = TodoItem.objects.all() 
    return render(request, "ViewTodo/viewtodo.html", { "jadwal": todos } )