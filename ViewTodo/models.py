from django.db import models
from django.utils import timezone

# Create your models here.

class TodoItem(models.Model):
    namaTodo = models.CharField(
        max_length=250
    )
    dateTodo = models.DateTimeField()
    waktu = models.TimeField()
    tempatTodo =  models.CharField(
        max_length=100
    )
    category_choice = [
        ('Bermain', 'Bermain'),
        ('Belajar', 'Belajar'),
        ('Nugas', 'Nugas'),
        ('Tambahan', 'Tambahan'),
    ]
    category = models.CharField(
        max_length=200 , choices=category_choice)
        

    def __str__(self):
        return self.namaTodo

    def date(self):
        return self.dateTodo.strftime('%B %d %Y')