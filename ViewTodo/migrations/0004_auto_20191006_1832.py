# Generated by Django 2.1.1 on 2019-10-06 11:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ViewTodo', '0003_auto_20191006_1831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoitem',
            name='dateTodo',
            field=models.DateTimeField(),
        ),
    ]
