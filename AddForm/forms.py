from django import forms
from ViewTodo.models import TodoItem
from datetime import datetime

class TodoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["category"].widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = TodoItem
        fields="__all__"
        widgets= {
            'namaTodo': forms.TextInput(attrs={'class' : 'form-control'}),
            'dateTodo' : forms.SelectDateWidget(attrs={'class': 'form-control'}),
            'tempatTodo' : forms.TextInput(attrs={'class': 'form-control'}),
            'waktu' : forms.TimeInput(attrs={'class': 'form-control', 'placeholder' : 'hh:mm'}),
        }
        labels = {
            "namaTodo": "Nama",
            'dateTodo': "Tanggal",
            "tempatTodo": "Tempat",
            "category" : "Kategori",
            "waktu" : "Waktu",
        }

    # def clean_dateTodo(self):
    #     my_date = self.cleaned_data.get('dateTodo')
    #     my_time = self.cleaned_data.get('waktu')
    #     if datetime.now() >= my_date:
    #         self.add_error('Invalid')
    #     return my_time
    
