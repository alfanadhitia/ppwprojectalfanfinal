from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from AddForm.views import AddTodo,deleteTodo


urlpatterns = [
    re_path(r'^$', AddTodo , name="AddTodo"),
    re_path(r'^deleteTodo/(?P<delete_id>\d+)/$', deleteTodo , name='deleteTodo')
]   