from django.shortcuts import render,redirect,HttpResponseRedirect
from ViewTodo.models import TodoItem
from .forms import TodoForm
from django.contrib import messages 
# Create your views here.


def AddTodo (request): 
    todos = TodoItem.objects.all() 
    data = TodoForm(request.POST)
    if request.method == "POST" and data.is_valid(): 
        saved = data.save()
        return redirect("/viewtodo") 
    else:
        messages.error(request, "Error")

    return render(request, "AddForm/form.html", {"todos": TodoForm(), "jadwal": todos } )

def deleteTodo(requst , delete_id):
    TodoItem.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("/viewtodo")



